<?php
/*
Plugin Name: Eastern Standard WordPress Post List
Plugin URI: https://bitbucket.org/estrnstd/eastern-standard-wordpress-post-list
Description: A WordPress plugin for building custom post lists.
Version: 1.1.1
Author: Eastern Standard
Author URI: https://easternstandard.com
Update URI: eswp-post-list
*/

require_once(ABSPATH . 'wp-admin/includes/plugin.php');

#[AllowDynamicProperties]
class ESWP_Post_List_Plugin {

	public $plugin_data;
	public $name;
	public $slug;
	public $version;
	public $info_url;
	public $cache_key;
	public $cache_allowed;
	public $cache_length;
	public $directory_public;
	public $plugin_dependencies;
	public $missing_dependencies;
	public $post_lists;
	public $default_results_object;

	public function __construct() {
		$this->plugin_data = get_plugin_data(__FILE__);
		$this->name = $this->plugin_data['Name'];
		$this->slug = plugin_basename(__DIR__);
		$this->version = $this->plugin_data['Version'];
		$this->allow_dashboard_updating = true;
		$this->info_url = 'https://bitbucket.org/estrnstd/eastern-standard-wordpress-post-list/raw/HEAD/info.json';
		$this->cache_key = $this->slug;
		$this->cache_allowed = true;
		$this->cache_length = DAY_IN_SECONDS;
		$this->directory = plugin_dir_url(__FILE__);
		$this->directory_public = '/wp-content/plugins/eswp-post-list/';
		$this->plugin_dependencies = [
			[
				'name' => 'Advanced Custom Fields Pro',
				'file' => 'advanced-custom-fields-pro/acf.php',
				'link' => 'https://www.advancedcustomfields.com/pro',
			],
			[
				'name' => 'Eastern Standard WordPress Utilities',
				'file' => 'eswp-utilities/eswp-utilities.php',
				'link' => 'https://bitbucket.org/estrnstd/eastern-standard-wordpress-utilities',
			],
		];
		$this->missing_dependencies = [];
		$this->post_lists = [];
		$this->default_results_object = [
			'all_results' => [],
			'results' => [],
			'results_start' => 0,
			'results_end' => 0,
			'total_pages' => 0,
			'page' => 0,
		];

		if ($this->allow_dashboard_updating) {
			$this->update_hooks();
		}
		$this->admin_enqueue();
		$this->check_dependencies();
	}

	// Updates

	public function update_hooks() {
		add_filter('plugins_api', [$this, 'plugins_api_filter'], 20, 3);
		add_filter('site_transient_update_plugins', [$this, 'site_transient_update_plugins_filter']);
		add_action('upgrader_process_complete', [$this, 'delete_cache'], 10, 2);
	}

	public function get_info() {
		$info = get_transient($this->cache_key);
		if ($info === false || !$this->cache_allowed) {
			$remote_info = wp_remote_get($this->info_url, [
				'timeout' => 10,
				'headers' => [
					'Accept' => 'application/json'
				],
			]);
			if (
				is_wp_error($remote_info)
				|| wp_remote_retrieve_response_code($remote_info) !== 200
				|| empty(wp_remote_retrieve_body($remote_info))
			) {
				return false;
			}
			$info = $remote_info;
			set_transient($this->cache_key, $remote_info, $this->cache_length);
		}
		$info = json_decode(wp_remote_retrieve_body($info));
		return $info;
	}

	public function plugins_api_filter($result, $action, $args) {
		// Do nothing if you're not getting plugin information right now.
		if($action !== 'plugin_information') {
			return $result;
		}
		// Do nothing if it's not our plugin.
		if($args->slug !== $this->slug) {
			return $result;
		}
		// Get updates.
		$info = $this->get_info();
		if (!$info) {
			return $result;
		}
		$result = new stdClass();
		$result->name = $info->name;
		$result->slug = $info->slug;
		$result->version = $info->version;
		$result->tested = $info->tested;
		$result->requires = $info->requires;
		$result->author = $info->author;
		$result->author_profile = $info->author_profile;
		$result->download_link = $info->download_url;
		$result->trunk = $info->download_url;
		$result->requires_php = $info->requires_php;
		$result->last_updated = $info->last_updated;
		$result->sections = [
			'description' => $info->sections->description,
			'installation' => $info->sections->installation,
			'changelog' => $info->sections->changelog
		];
		if (!empty($info->banners)) {
			$result->banners = [
				'low' => $info->banners->low,
				'high' => $info->banners->high
			];
		}
		return $result;
	}

	public function site_transient_update_plugins_filter($transient) {
		if (empty($transient->checked)) {
			return $transient;
		}
		$info = $this->get_info();
		if (
			$info
			&& version_compare($this->version, $info->version, '<')
			&& version_compare($info->requires, get_bloginfo('version'), '<')
			&& version_compare($info->requires_php, PHP_VERSION, '<')
		) {
			$result = new stdClass();
			$result->slug = $this->slug;
			$result->plugin = plugin_basename(__FILE__);
			$result->new_version = $info->version;
			$result->tested = $info->tested;
			$result->package = $info->download_url;
			$transient->response[$result->plugin] = $result;
		}
		return $transient;
	}

	public function upgrader_process_complete_action($upgrader_object, $options) {
		if ($options['action'] === 'update' && $options['type'] === 'plugin') {
			$this->delete_cache();
		}
	}

	public function delete_cache() {
		delete_transient($this->cache_key);
	}

	// Admin enqueue

	public function admin_enqueue() {
		add_action('admin_enqueue_scripts', function () {
			wp_enqueue_style($this->slug, $this->directory_public . 'dist/admin.css');
		});
	}

	// Dependencies

	public function check_dependencies() {

		foreach ($this->plugin_dependencies as $plugin_dependency) {
			if (!is_plugin_active($plugin_dependency['file'])) {
				array_push($this->missing_dependencies, $plugin_dependency);
			}
		}

		if (count($this->missing_dependencies) > 0) {
			$this->deactivate();
			add_action('admin_notices', function() {
				$notice_output = '<div class="wrap ' . $this->slug . '-admin-notice">';
					$notice_output .= 'Plugin: ' . $this->name . ' has been deactivated because it requires';
					if (count($this->missing_dependencies) <= 1) {
						$dependency = $this->missing_dependencies[0];
						$notice_output .= ' plugin: <a href="' . $dependency['link'] . '" target="_blank">';
							$notice_output .= $dependency['name'];
						$notice_output .= '</a> to be installed and activated.';
					}
					else {
						$notice_output .= 'the following plugins to be installed and activated:';
						$notice_output .= '<ul>';
							foreach ($this->missing_dependencies as $dependency) {
								$notice_output .= '<li>';
									$notice_output .= '<a href="' . $dependency['link'] . '" target="_blank">';
										$notice_output .= $dependency['name'];
									$notice_output .= '</a>';
								$notice_output .= '</li>';
							}
						$notice_output .= '</ul>';
					}
				$notice_output .= '</div>';
				echo $notice_output;
			});
		}	

	}

	public function has_missing_dependencies() {
		if (count($this->missing_dependencies) > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	public function deactivate() {
		deactivate_plugins(plugin_basename(__FILE__));
		if (isset($_GET['activate'])) {
			unset($_GET['activate']);
		}
	}

	//

	public function register_post_list($options) {
		$key = $options['key'];
		if (!isset($this->post_lists[$key])) {
			$this->post_lists[$key] = $options;
		}
		else {
			trigger_error(__("Post list key '{$key}' already exists. A post list key can only be used once."), E_USER_WARNING);
		}
	}

	public function get_post_list($key) {
		if (isset($this->post_lists[$key])) {
			return $this->post_lists[$key];
		}
	}

}

$GLOBALS['eswp_post_list_plugin'] = new ESWP_Post_List_Plugin();
global $eswp_post_list_plugin;

if (!$eswp_post_list_plugin->has_missing_dependencies()) {

	function eswp_post_lists_initialize($post_lists_directory) {
		global $eswp_post_list_plugin;
		if (substr($post_lists_directory, -1, 1) !== '/') {
			$post_lists_directory .= '/';
		}
		$post_list_directories = $post_lists_directory . '*/';
		$post_list_directories = glob($post_list_directories);
		unset($eswp_post_list_options);
		foreach ($post_list_directories as $post_list_directory) {
			$post_list_directory_config_file_path = $post_list_directory . 'config.php';
			$post_list_directory_public = eswp_get_public_path($post_list_directory);
			include $post_list_directory_config_file_path;
			if (isset($eswp_post_list_options) && is_array($eswp_post_list_options)) {
				if (!isset($eswp_post_list_options['key']) || !is_string($eswp_post_list_options['key'])) {
					trigger_error(__("\$eswp_post_list_options['key'] in {$post_list_directory_config_file_path} must be a string."), E_USER_WARNING);
				}
				if (!isset($eswp_post_list_options['post_list_template']) || !is_string($eswp_post_list_options['post_list_template'])) {
					trigger_error(__("\$eswp_post_list_options['post_list_template'] in {$post_list_directory_config_file_path} must be a string."), E_USER_WARNING);
				}
				$eswp_post_list_plugin->register_post_list($eswp_post_list_options);
			}
			else {
				trigger_error(__("\$eswp_post_list_options in {$post_list_directory_config_file_path} must be an array."), E_USER_WARNING);
			}
			unset($eswp_post_list_options);
		}
	}
	
	function eswp_post_list($key, $additional_data = null) {
		global $eswp_post_list_plugin;

		$enqueue_post_list_js = function () use($eswp_post_list_plugin) {
			wp_enqueue_script($eswp_post_list_plugin->slug, $eswp_post_list_plugin->directory_public . 'dist/eswp-post-list.js', ['web-components-polyfill']);
			wp_localize_script($eswp_post_list_plugin->slug, 'eswp_post_list', [
				'ajax_url' => admin_url('admin-ajax.php'),
			]);
		};
		if (is_admin()) {
			add_action('admin_enqueue_scripts', $enqueue_post_list_js);
		}
		else {
			call_user_func($enqueue_post_list_js);
		}

		$results_object = $eswp_post_list_plugin->default_results_object;
		$options = $eswp_post_list_plugin->get_post_list($key);
		if ($options['initial_results']) {
			$results_object = eswp_post_list_get_results($key);
		}
		if ($options['enqueue_assets'] && is_callable($options['enqueue_assets'])) {
			if (is_admin()) {
				add_action('admin_enqueue_scripts', $options['enqueue_assets']);
			}
			else {
				call_user_func($options['enqueue_assets']);
			}
		}

		$render_post_list_template = function ($template) use($key, $results_object, $additional_data) {
			$args = [
				'key' => $key,
				'templates' => [$template],
				'results_object' => $results_object,
				'additional_data' => $additional_data,
			];
			eswp_render_templates($args);
		};

		include $options['post_list_template'];
	}

	/**
	 * eswp_post_list_get_results
	 * 
	 * @param   string   $key
	 * @param   array    $request_data
	 * @param   int      $page
	 * 
	 * @return  mixed[]  [
	 *   'all_results' => [],
	 *   'results' => [],
	 *   'results_start' => 0,
	 *   'results_end' => 0,
	 *   'total_pages' => 0,
	 *   'page' => 0,
	 * ];
	 */

	function eswp_post_list_get_results($key, $request_data = [], $page = 1) {
		global $eswp_post_list_plugin;
		$options = $eswp_post_list_plugin->get_post_list($key);

		$results_object = $eswp_post_list_plugin->default_results_object;

		$wp_query_args = [
			'post_type' => 'any',
			'posts_per_page' => -1,
		];
		if (isset($options['wp_query_args'])) {
			$wp_query_args = $options['wp_query_args']($request_data);
		}
		$pl_wp_query = new WP_Query($wp_query_args);
		$results_object['all_results'] = $pl_wp_query->get_posts();

		if (isset($options['custom_filter'])) {
			$custom_filtered_results = [];
			foreach ($results_object['all_results'] as $result) {
				if ($options['custom_filter']($result, $request_data)) {
					array_push($custom_filtered_results, $result);
				}
			}
			$results_object['all_results'] = $custom_filtered_results;
		}

		if (isset($options['custom_sort'])) {
			$custom_sort = $options['custom_sort']($request_data);
			usort($results_object['all_results'], function ($a, $b) use($custom_sort) {
				return $custom_sort($a, $b);
			});
		}

		if (isset($options['results_offset']) && is_int($options['results_offset']) && $options['results_offset'] > 0) {
				$results_object['offset_results'] = array_slice($results_object['all_results'], $options['results_offset']);
		}
		else {
			$results_object['offset_results'] = $results_object['all_results'];
		}

		$results_object['results'] = $results_object['offset_results'];

		$posts_per_page = 10;
		if (isset($options['posts_per_page']) && is_int($options['posts_per_page'])) {
			$posts_per_page = $options['posts_per_page'];
		}
		$results_object['total_pages'] = intval(ceil(count($results_object['results']) / $posts_per_page));

		$results_object['page'] = intval($page);

		$results_object['results_start'] = 0;
		$results_object['results_end'] = count($results_object['results']) - 1;
		if ($posts_per_page > 0) {
			$results_object['results_start'] = ($posts_per_page * ($page - 1));
			if ($results_object['results_start'] > count($results_object['results']) - 1) {
				// If there are too few results to create the requested page.
				$results_object['results_start'] = null;
				$results_object['results_end'] = null;
				$results_object['results'] = [];
			}
			else {
				$results_object['results_end'] = ($posts_per_page * $page) - 1;
				$results_object['results'] = array_slice($results_object['results'], $results_object['results_start'], $posts_per_page);
				if (count($results_object['results']) < $posts_per_page) {
					$results_object['results_end'] = $results_object['results_end'] - ($posts_per_page - count($results_object['results']));
				}
			}
		}

		return $results_object;
	}

	add_action('wp_ajax_eswp_render_templates', 'eswp_render_templates');
	add_action('wp_ajax_nopriv_eswp_render_templates', 'eswp_render_templates');

	/**
	 * eswp_render_templates
	 * 
	 * @param  mixed[]  $args  An array of arguments.
	 *   $args = [
	 *     'key'              =>  (string)  Required.
	 *     'templates'        =>  (array)   Required.
	 *     'request_data'     =>  (mixed)   Optional. Defaults to an empty array.
	 *     'page'             =>  (int)     Optional. Defaults to 1.
	 *     'results_object'   =>  (array)   Optional. Defaults querying for results.
	 *     'language'         =>  (string)  Optional. Language code.
	 *     'additional_data'  =>  (array)   Optional. Additional data.
	 *   ]
	 */

	function eswp_render_templates($args) {
		$is_ajax = false;
		if (isset($_POST) && isset($_POST['action']) && $_POST['action'] === 'eswp_render_templates') {
			$is_ajax = true;
			$args_string = str_replace('\"', '"', $_POST['args']);
			$args = json_decode($args_string, true);
		}

		$additional_data = $args['additional_data'];

		if (array_key_exists('language', $args) && $args['language']) {
			do_action('wpml_switch_language', $args['language']);
		}

		if (!array_key_exists('key', $args) || !isset($args['key'])) {
			trigger_error(__("Function 'eswp_render_templates' argument \$args['key'] must be a string."), E_USER_WARNING);
		}
		if (!array_key_exists('templates', $args) || !isset($args['templates'])) {
			trigger_error(__("Function 'eswp_render_templates' argument \$args['templates'] must be an array of strings."), E_USER_WARNING);
		}
		if (!array_key_exists('request_data', $args) || !isset($args['request_data'])) {
			$args['request_data'] = [];
		}
		if (!array_key_exists('page', $args) || !isset($args['page'])) {
			$args['page'] = 1;
		}
		if (!array_key_exists('results_object', $args) || !isset($args['results_object'])) {
			$args['results_object'] = eswp_post_list_get_results($args['key'], $args['request_data'], $args['page']);
		}

		$rendered_templates = [];
		global $eswp_post_list_plugin;
		$options = $eswp_post_list_plugin->get_post_list($args['key']);
		$results_object = $args['results_object'];
		$request_data = $args['request_data'];
		if (isset($options['templates'])) {
			foreach ($args['templates'] as $template_key) {
				if (isset($options['templates'][$template_key])) {
					if ($is_ajax) {
						ob_start();
					}
					include $options['templates'][$template_key];
					if ($is_ajax) {
						$rendered_templates[$template_key] = ob_get_contents();
						ob_end_clean();
					}
					else {
						array_push($rendered_templates, $template_key);
					}
				}
			}
		}

		if ($is_ajax) {
			$data = json_encode($rendered_templates);
			echo $data;
			die();
		}
		else {
			return $rendered_templates;
		}
	}

}
