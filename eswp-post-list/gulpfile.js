const gulp = require('gulp');
const glob = require('glob');
const path = require('path');
const sass = require('gulp-sass')(require('sass'));
const mergeStream = require('merge-stream');

const styles = () => {
	const streams = [];
	const scssEntries = glob.sync('./src/*.scss');
	for (const scssEntryIndex in scssEntries) {
		const scssEntry = scssEntries[scssEntryIndex];
		const stream = gulp.src(scssEntry)
			.pipe(sass().on('error', sass.logError))
			.pipe(gulp.dest(path.resolve('./dist/')));
		streams.push(stream);
	}
	return mergeStream(...streams);
}

const watchStyles = () => {
	gulp.watch('./src/*.scss', gulp.parallel(styles));
};

exports.default = gulp.series(styles, watchStyles);