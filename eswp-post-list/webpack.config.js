const path = require('path');

const webpackConfig = {
	entry: './src/eswp-post-list.js',
	target: 'web',
	output: {
		library: 'ESWPPostList',
		libraryTarget: 'var',
		filename: 'eswp-post-list.js',
		path: path.resolve('./dist/')
	}
};

module.exports = webpackConfig;