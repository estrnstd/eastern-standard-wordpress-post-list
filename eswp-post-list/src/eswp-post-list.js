const extend = require('@alexspirgel/extend');

class ESWPPostList extends HTMLDivElement {

	static isAdmin() {
		if (typeof wp === 'object' && typeof wp.blockEditor !== 'undefined') {
			return true;
		}
		else {
			return false;
		}
	}

	static get ajaxURL() {
		return eswp_post_list.ajax_url;
	}

	static getFormValues(element) {
		const formFieldNamesAndValues = {};
		const formData = new FormData(element);
		for (const entry of formData.entries()) {
			const name = entry[0];
			const value = entry[1];
			formFieldNamesAndValues[name] = value;
		}
		return formFieldNamesAndValues;
	}

	static setFormValues(formElement, namesValues) {
		for (const name in namesValues) {
			const value = namesValues[name];
			const formFieldElement = formElement.querySelector(`[name="${name}"]`);
			if (formFieldElement) {
				if (formFieldElement.tagName === 'SELECT') {
					let foundOption = null;
					const options = formFieldElement.querySelectorAll('option');
					for (const option of options) {
						const optionValue = option.getAttribute('value');
						if (value === optionValue) {
							foundOption = true;
							break;
						}
					}
					if (foundOption) {
						formFieldElement.value = value;
					}
				}
				else {
					formFieldElement.value = value;
				}
			}
		}
	}

	static getQueryStrings() {
		const queryStrings = {};
		const urlSearchParams = new URLSearchParams(window.location.search);
		for (const urlSearchParam of urlSearchParams) {
			const key = urlSearchParam[0];
			const value = urlSearchParam[1];
			if (typeof key !== undefined) {
				queryStrings[key] = '';
				if (typeof value !== undefined) {
					queryStrings[key] = value;
				}
			}
		}
		return queryStrings;
	}

	static setQueryStrings(queryStrings) {
		const searchParams = new URLSearchParams(window.location.search);
		for (const key in queryStrings) {
			const value = queryStrings[key];
			searchParams.set(key, value);
		}
		const newURL = new URL(window.location.href);
		newURL.search = searchParams.toString();
		window.history.replaceState({path: newURL.href}, '', newURL.href);
	}

	constructor() {
		super();
	}
	
	get templateElements() {
		const keyedTemplateElements = {};
		const templateElements = this.querySelectorAll('[data-post-list-template]');
		for (const templateElement of templateElements) {
			const templateKey = templateElement.getAttribute('data-post-list-template');
			if (templateKey) {
				keyedTemplateElements[templateKey] = templateElement;
			}
		}
		return keyedTemplateElements;
	}

	hasTemplateElement(templateKey) {
		if (typeof this.templateElements[templateKey] !== 'undefined') {
			return true;
		}
		else {
			return false;
		}
	}

	get updateTemplateElementCallbacks() {
		if (typeof this._afterAddTemplateElementCallbacks !== 'object') {
			this._afterAddTemplateElementCallbacks = {};
		}
		return this._afterAddTemplateElementCallbacks;
	}

	addUpdateTemplateElementCallback(templateKey, callback) {
		const existingTemplateCallbacks = this.updateTemplateElementCallbacks[templateKey];
		if (!Array.isArray(existingTemplateCallbacks)) {
			this.updateTemplateElementCallbacks[templateKey] = [];
		}
		this.updateTemplateElementCallbacks[templateKey].push(callback);
	}

	updateTemplateElement(templateKey, replacementValue) {
		const oldTemplate = this.templateElements[templateKey];
		if (!oldTemplate) {
			throw new Error(`When trying to replace template, existing template '${templateKey}' does not exist.`);
		}
		oldTemplate.outerHTML = replacementValue;
		const templateUpdateCallbacks = this.updateTemplateElementCallbacks[templateKey];
		if (Array.isArray(templateUpdateCallbacks)) {
			for (const templateUpdateCallback of templateUpdateCallbacks) {
				templateUpdateCallback(templateKey);
			}
		}
	}

	get key() {
		return this.getAttribute('data-post-list');
	}

	async ajaxRequest(action, args) {
		const body = new FormData();
		body.append('action', action);
		body.append('args', JSON.stringify(args));
		const response = await fetch(this.constructor.ajaxURL, {
			method: 'POST',
			credentials: 'same-origin',
			body: body
		});
		if (response.ok) {
			const json = await response.json();
			this.recordRequestAndResponse({
				action: action,
				args: args
			}, json);
			return json;
		}
		else {
			throw new Error(response.status + ' ' + response.statusText + ' ' + response.url);
		}
	}

	get requestsAndResponses() {
		if (!Array.isArray(this._requestsAndResponses)) {
			this._requestsAndResponses = [];
		}
		return this._requestsAndResponses;
	}

	recordRequestAndResponse(request, response) {
		const requestAndResponse = {
			request: request,
			response: response
		};
		this.requestsAndResponses.push(requestAndResponse);
	}

	async fetchTemplates(argsInput) {
		const action = 'eswp_render_templates';
		const argsDefault = {
			key: null,
			templates: [],
			request_data: {},
			page: 1
		};
		const args = extend({}, argsDefault, argsInput);
		const responseData = await this.ajaxRequest(action, args);
		return responseData;
	}

}

module.exports = ESWPPostList;