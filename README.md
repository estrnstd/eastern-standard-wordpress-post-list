# Eastern Standard WordPress Post List
A WordPress plugin for building custom post lists.

## Installation
Copy the contents of the `eswp-post-list` directory from this repository into the plugins directory. The path should look like this: `/wp-content/plugins/eswp-post-list/eswp-post-list.php`.

Activate the plugin via the WordPress dashboard 'Plugins' page (`/wp-admin/plugins.php`).

## Usage
Documentation needed.

## Development
Do not include `node_modules` in the `.zip`.

To-do:
* Add support for `$additional_data` to be passed into ajax template renders.